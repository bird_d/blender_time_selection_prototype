    # Procedural Fcurve Editor, Adds tools to make modifying fcurves easier.
    # Copyright (C) 2020 bird_d <relay198@gmail.com>

    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.

    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.

    # You should have received a copy of the GNU General Public License
    # along with this program.  If not, see <https://www.gnu.org/licenses/>.

bl_info = {
    "name": "Time Selection Concept/Prototype",
    "author": "bird_d",
    "version": (1, 0, 0),
    "blender": (2, 83, 0),
    "location": "Animation Editors",
    "description": "Concept/Prototype for Time Selection in Blender",
    "warning": "",
    "doc_url": "",
    "category": "Animation",
}

#Reload modules if we're reloading the add-on
if "bpy" in locals():
    import importlib
    importlib.reload(utils)
else:
    from .utils import *

from mathutils import Vector
import bpy
import gpu
import random
from gpu_extras.batch import batch_for_shader
import bgl
import blf
from bgl import *
from bpy.types import AddonPreferences
from bpy.props import (
    IntProperty, 
    FloatProperty,
    FloatVectorProperty,
    BoolProperty, 
    EnumProperty, 
    StringProperty,
    PointerProperty
)

draw_debug = True

interpolation_modes = [
    ('LINEAR', 'Linear', '', 'IPO_LINEAR', 0),
    ('SINE', 'Sinusoidal', '', 'IPO_SINE', 1),
    ('QUAD', 'Quadric', '', 'IPO_QUAD', 2),
    ('CUBIC', 'Cubic', '', 'IPO_CUBIC', 3),
    ('QUART', 'Quartic', '', 'IPO_QUART', 4),
    ('QUINT', 'Quintic', '', 'IPO_QUINT', 5),
    ('EXPO', 'Exponential', '', 'IPO_EXPO', 6),
    ('CIRC', 'Circular', '', 'IPO_CIRC', 7),
    ('BACK', 'Back', '', 'IPO_BACK', 8),
    ('ELASTIC', 'Elastic', '', 'IPO_ELASTIC', 9),
    ('BOUNCE', 'Bounce', '', 'IPO_BOUNCE', 10)
]
easing_types = [
    ('IN', 'In', '', 'IPO_EASE_IN', 0),
    ('OUT', 'Out', '', 'IPO_EASE_OUT', 1),
    ('IN_OUT', 'In and Out', '', 'IPO_EASE_IN_OUT', 2)
]

class ProceduralFcurve_Settings(AddonPreferences):
    # this must match the addon name, use '__package__'
    # when defining this in a submodule of a python package.
    bl_idname = __name__

    interpolation_type : EnumProperty(
        items=interpolation_modes,
        name="Interpolation Type",
        default='LINEAR'
    )
    easing_type : EnumProperty(
        items=easing_types,
        name="Easing Type",
        default='IN'
    )
    sync_preview_range : BoolProperty(
        default=False, 
        description="Automatically set the preview range when the procedural fcurve range updates")

    deselect_new_keyframes : BoolProperty(
        default=True, 
        description="Deselects the keyframe handles during an fcurve operators' modal")

    draw_debug : BoolProperty(
        default=False,
        description="Show debug drawing for the Time Range"
    )

    show_hover_handle_influence : BoolProperty(
        default=True,
        description="Show the influence under the green handle"
    )

    show_selected_handles_frame : BoolProperty(
        default=True,
        description="Show the frame on selected handles, similar to how the playhead is drawn"
    )

    use_beauty_drawing : BoolProperty(
        default=True,
        description="Draw the influence line with anti-aliasing"
    )

    show_hover_handle : BoolProperty(
        default=True,
        description="Show the handle that appears when holding shift"
    )

    def draw(self, context):
        layout = self.layout
        box = layout.box()
        box.prop(self, "draw_debug", text="Debug Draw")
        box.prop(self, "show_hover_handle", text="Show Frame Hover Scrubber")
        box.prop(self, "show_hover_handle_influence", text="Show the influence value on the frame hover scrubber")
        box.prop(self, "show_selected_handles_frame", text="Show Selected Handle(s) Frame")
        box.prop(self, "use_beauty_drawing", text="Use Beauty Drawing")
        
def toggle_pfc_edit_mode(self, context):
    if context.scene.procedural_graph.edit_mode == True:
        bpy.ops.timerange.procedural_graph_editmode('INVOKE_DEFAULT')
        return
    pgh = context.scene.procedural_graph_handler
    if pgh.start == pgh.end:
        pgh.disable()

class ProceduralGraph_Settings(bpy.types.PropertyGroup):
    edit_mode : BoolProperty(
        name="Procedural Range Edit Mode",
        default=True,
        description="Go in to editing the procedural fcurve range",
        update=toggle_pfc_edit_mode
    )
    #HACK: This is for being able to transform something using the
    # transform shortcuts without having access to blender's internals
    offset_start : FloatVectorProperty()
    offset : FloatVectorProperty()
    fcurves = []
    fghosts = []
    hcurves = []
    ecurves = []
    key_hold = []
    play_hold : IntProperty()
    auto_key_hold : IntProperty()

class TimeRangeHandle():
    select = False
    frame = 0

class TimeRange():
    handles = [
        TimeRangeHandle(),
        TimeRangeHandle(),
        TimeRangeHandle(),
        TimeRangeHandle()
    ]
    start = 0
    end = 0

class Procedural_Graph_Handler():
    def __init__(self):
        self.range = TimeRange()
        self.start = 0
        self.end = 0
        self.falloff_left_interpolation = 'LINEAR'
        self.falloff_left_interpolation_index = 0
        self.falloff_left_easing = 'IN'
        self.falloff_left_easing_index = 0
        self.falloff_right_interpolation = 'LINEAR'
        self.falloff_right_interpolation_index = 0
        self.falloff_right_easing = 'IN'
        self.falloff_right_easing_index = 0
        self.enabled = False
        self.mouse_x = 0
        self.mouse_y = 0
    def draw(self):
        #TODO: Change draw_box_from_points and other stuff add to a vert buffer instead
        # So we can cut the draw calls down.
        context = bpy.context
        area = context.area
        #Draw a slight color on the background if we're in edit_mode
        if context.scene.procedural_graph.edit_mode:
            draw_box_from_points(
                0, 0, 
                area.width, area.height-49, 
                (0.33, 0.95, 0.33, 0.05)
            )
        if self.start == self.end:
            return

        #Can just use an array for this, since handles is an array now
        (falloff_start_x, _) = context.region.view2d.view_to_region(self.range.handles[0].frame, 0, clip=False)
        (frame_start_x,   _) = context.region.view2d.view_to_region(self.range.handles[1].frame, 0, clip=False)
        (frame_end_x,     _) = context.region.view2d.view_to_region(self.range.handles[2].frame, 0, clip=False)
        (falloff_end_x,   _) = context.region.view2d.view_to_region(self.range.handles[3].frame, 0, clip=False)

        handles_x_pixels = []
        for h in self.range.handles:
            (x, _) = context.region.view2d.view_to_region(h.frame, 0, clip=False)
            handles_x_pixels.append(x)

        col = (0.33, 0.75, 0.33, 0.33)
        col2 = (0.33, 0.75, 0.33, 0.15)
        handle_col = (0, 0, 0, 0.75)
        handle_sel_col = (1.0, 0.521569, 0.0, 1.0)

        #Hard-coded size of the graph editors vertical bars' positions
        bottom = 0
        top = area.height-49

        #Draw the gradient box from handle 1 to 2, and the black line for 1
        if self.range.handles[0].frame < self.range.handles[1].frame:
            draw_gradient_box_from_points(
                falloff_start_x, bottom, 
                frame_start_x, top, 
                col, col2)
            # draw_box_from_points(falloff_start_x, bottom, falloff_start_x+1, top, handle_col)
        #Draw solid box from handle 2 to 3
        draw_box_from_points(frame_start_x, bottom, frame_end_x, top, col)
        #Same as above but for 3 to 4, line on 4
        if self.range.handles[3].frame > self.range.handles[2].frame:
            draw_gradient_box_from_points(
                frame_end_x, bottom, 
                falloff_end_x, top, 
                col2, col)
            # draw_box_from_points(falloff_end_x, bottom, falloff_end_x+1, top, handle_col)

        # TODO: For C implementation, make sure that handle selection is merged
        #   when dragged on to another handle, so that drawing can be correct
        # Draw the handles
        for i in range(0, len(self.range.handles)):
            handle = self.range.handles[i]
            x = handles_x_pixels[i]

            #Skip drawing left falloff handle if not less than frame start handle
            if i == 0:
                if not (self.range.handles[0].frame < self.range.handles[1].frame):
                    continue
            
            # # Skip drawing 1 of the two middle handle if they have the same frame
            # if i == 1:
            #     if self.range.handles[1].frame == self.range.handles[2].frame:
            #         continue

            #Skip drawing right falloff handle if not greater than frame end handle
            if i == 3:
                if not (self.range.handles[3].frame > self.range.handles[2].frame):
                    continue

            #Draw handle with selection color or not
            if handle.select:
                if context.scene.procedural_graph.edit_mode and get_preferences().show_selected_handles_frame:
                    draw_handle_selection(handle.frame, handle_sel_col, context)
                else:
                    draw_box_from_points(x, bottom, x+1, top, handle_sel_col)
            else:
                draw_box_from_points(x, bottom, x+1, top, handle_col)

        #Draw areas for debugging clickable area ranges
        if get_preferences().draw_debug and (context.scene.procedural_graph.edit_mode):
            def draw_debug_range(self):
                #Draw Influence area ctrl scrollwheel box
                if is_in_rect((self.mouse_x, self.mouse_y), falloff_start_x, top, frame_start_x, area.height):
                    draw_box_from_points(falloff_start_x, top, frame_start_x, area.height, col)

                if is_in_rect((self.mouse_x, self.mouse_y), frame_end_x, top, falloff_end_x, area.height):
                    draw_box_from_points(frame_end_x, top, falloff_end_x, area.height, col)

                #Text
                tmp = []
                found_handles = find_closest_time_range_handles(self.mouse_x, self.range, handles_x_pixels)
                for i in range(0, len(self.range.handles)):
                    handle = self.range.handles[i]
                    if handle in found_handles:
                        tmp.append((i, handle.frame, handle.select))

                font_id = 0

                size, dpi = (36, 36)
                blf.size(font_id, size, dpi)
                width, height = blf.dimensions(font_id, "Handles : %s" % tmp)
                font_x = 0
                font_y = area.height - 64
                blf.position(font_id, font_x, font_y-1, 0)
                blf.color(font_id, 1.0, 1.0, 1.0, 1.0)
                blf.draw(font_id, "Handles : %s" % tmp)

            draw_debug_range(self)
            

        #Falloff guide
        shader = gpu.shader.from_builtin('2D_SMOOTH_COLOR')
        
        bgl.glEnable(bgl.GL_BLEND)
        if get_preferences().use_beauty_drawing:
            bgl.glEnable(bgl.GL_LINE_SMOOTH)
        verts = []
        cols = []
        for frame in range(self.start, self.end+1):
            #No points need to be computed between the influence range that is always 1,
            # it will stretch
            if self.range.handles[1].frame < frame < self.range.handles[2].frame:
                continue
            # y gets influenced by the value of easing, but not x
            frm_infl = self.get_frame_influence(frame)
            y = scale(frm_infl, area.height-47, area.height-27)
            #Stretch normalized value to falloff and frame's start and end 
            # position within the viewport
            infl = normalize(frame, self.start, self.end)
            x = scale(infl, falloff_start_x, falloff_end_x)
            verts.append((x, y))
            cols.append((0.5, 1, 0.5, max(0.15, frm_infl/3)))

        fmt = gpu.types.GPUVertFormat()
        fmt.attr_add(id = "pos", comp_type = 'F32', len = 2, fetch_mode = 'FLOAT')
        fmt.attr_add(id = "color", comp_type = 'F32', len = 4, fetch_mode = 'FLOAT')

        vbo = gpu.types.GPUVertBuf(len=len(verts), format=fmt)
        vbo.attr_fill(id="pos", data=verts)
        vbo.attr_fill(id="color", data=cols)
        
        batch = gpu.types.GPUBatch(type='LINE_STRIP', buf = vbo)
        shader.bind()
        # shader.uniform_float("color", (0.5, 1, 0.5, 0.15))
        batch.draw(shader)
        #Defaults
        bgl.glDisable(bgl.GL_LINE_SMOOTH)
        bgl.glDisable(bgl.GL_BLEND)

        #draw_selection_area(self.range.handles[1].frame, self.range.handles[2].frame)
    def enable(self):
        # print("PGHandler Enabled")
        self._handle = bpy.types.SpaceGraphEditor.draw_handler_add(
            self.draw, (), 
            'WINDOW', 
            'POST_PIXEL')
        self._handle_dopesheet = bpy.types.SpaceDopeSheetEditor.draw_handler_add(
            self.draw, (), 
            'WINDOW', 
            'POST_PIXEL')
        self.enabled = True
        for area in bpy.context.screen.areas:
            area.tag_redraw()
    def disable(self):
        # print("PGHandler Disabled")
        if self.enabled == False:
            return
        bpy.types.SpaceGraphEditor.draw_handler_remove(
            self._handle, "WINDOW")
        bpy.types.SpaceDopeSheetEditor.draw_handler_remove(
            self._handle_dopesheet, "WINDOW")
        
        self.enabled = False
        self.range.handles[0].frame = 0
        self.range.handles[1].frame = 0
        self.range.handles[2].frame = 0
        self.range.handles[3].frame = 0
        self.update_range()
        for area in bpy.context.screen.areas:
            area.tag_redraw()
    def check(self):
        if self.range.handles[1].frame == self.range.handles[2].frame:
            return True
        return None
    #Returns a normalized value based on what frame and where that 
    # lies between the frame handles, and then easing is applied
    def get_frame_influence(self, frame):
        if self.start == frame or frame == self.end:
            if frame == self.range.handles[0].frame == self.range.handles[1].frame:
                return 1
            if frame == self.range.handles[2].frame == self.range.handles[3].frame :
                return 1
            return 0
        if self.range.handles[1].frame <= frame <= self.range.handles[2].frame:
            return 1
        if self.range.handles[0].frame < frame < self.range.handles[1].frame:
            return self._get_falloff_influence(
                frame,
                normalize(frame, self.range.handles[0].frame, self.range.handles[1].frame))
        if self.range.handles[2].frame < frame < self.range.handles[3].frame:
            return self._get_falloff_influence(
                frame,
                normalize(frame, self.range.handles[3].frame, self.range.handles[2].frame))
        return 0

    #Really only should be called by functions within this class
    #It should be used for the normalized value as the easing 
    # functions only go from 0-1
    def _get_falloff_influence(self, frame, val):
        interpolation_type = 'LINEAR'
        easing_type = 'IN'

        if self.range.handles[0].frame <= frame <= self.range.handles[1].frame:
            interpolation_type = self.falloff_left_interpolation
            easing_type = self.falloff_left_easing

        if self.range.handles[2].frame <= frame <= self.range.handles[3].frame:
            interpolation_type = self.falloff_right_interpolation
            easing_type = self.falloff_right_easing

        if interpolation_type == 'LINEAR':
            return val

        #Use a string to call a function on the utils.py/Ease class we 
        # have imported. Far more maintainable than using an if statement
        return getattr(
            Ease, 
            "%s_%s" % (easing_type.lower(), interpolation_type.lower())) (val)

    def update_range(self):
        if self.range.handles[0].frame > self.range.handles[1].frame:
            self.range.handles[0].frame = self.range.handles[1].frame
        if self.range.handles[3].frame < self.range.handles[2].frame:
            self.range.handles[3].frame = self.range.handles[2].frame

        hold = self.range.handles[1].frame
        if self.range.handles[1].frame > self.range.handles[2].frame:
            self.range.handles[1].frame = self.range.handles[2].frame
        if self.range.handles[2].frame < hold:
            self.range.handles[2].frame = hold

        self.start = self.range.handles[1].frame
        if self.range.handles[0].frame < self.range.handles[1].frame:
            self.start = self.range.handles[0].frame
        self.end = self.range.handles[2].frame
        if self.range.handles[3].frame > self.range.handles[2].frame:
            self.end = self.range.handles[3].frame

        if get_preferences().sync_preview_range:
            scene = bpy.context.scene
            if self.start == self.end:
                scene.use_preview_range = False
                scene.frame_preview_start = scene.frame_preview_end = 0
            else:
                scene.use_preview_range = True
                scene.frame_preview_start = self.start
                scene.frame_preview_end = self.end

#I'm sorry for this abomination against nature
def transform_update():
    pg = bpy.context.scene.procedural_graph
    pgh = bpy.context.scene.procedural_graph_handler
    pg.offset = bpy.context.active_pose_bone.location - Vector(pg.offset_start)
    for i in range(0, len(pg.fcurves)):
        fc = pg.fcurves[i]
        ec = pg.ecurves[i]
        # print(fc.data_path, fc.array_index)
        for frame_index in range(pgh.start, pgh.end+1):
            insert_keyframe_point(
                fcurve=fc,
                frame=frame_index, 
                value=scale(
                            pgh.get_frame_influence(frame_index), 
                            ec[frame_index-pgh.start],
                            ec[frame_index-pgh.start] + pg.offset[fc.array_index]
                            ),
                options={"NEEDED"}, 
                keyframe_type="JITTER",
                deselect=True)
        insert_keyframe_point(
            fcurve=fc,
            frame=pgh.start, 
            value=pg.offset_start[fc.array_index] + pg.offset[fc.array_index],
            options={"NEEDED"}, 
            keyframe_type="JITTER",
            deselect=True)
    bpy.context.active_pose_bone.location = Vector(pg.offset_start) + Vector(pg.offset)
    return 0.0165 #About 60 updates per second

def draw_curves():
    context = bpy.context
    area = context.area
    shader = gpu.shader.from_builtin('2D_SMOOTH_COLOR')
    #Enable some gl context stuff
    bgl.glEnable(bgl.GL_BLEND)
    if area.spaces[0].use_beauty_drawing:
        bgl.glEnable(bgl.GL_LINE_SMOOTH)
    verts = []
    cols = []
    pg = context.scene.procedural_graph
    active_fghost = None
    for fghost in pg.fghosts:
        if context.active_editable_fcurve:
            # print(context.active_editable_fcurve, fghost.fcurve)
            if fghost.fcurve == context.active_editable_fcurve:
                active_fghost = fghost
                continue
        # print("FCURVE: ", fghost.fcurve.array_index)
        fghost.add_to_vert_buffer(verts, cols)
    #Make sure we draw active fcurve on top
    if active_fghost:
        active_fghost.add_to_vert_buffer(verts, cols)
    fmt = gpu.types.GPUVertFormat()
    fmt.attr_add(id = "pos", comp_type = 'F32', len = 2, fetch_mode = 'FLOAT')
    fmt.attr_add(id = "color", comp_type = 'F32', len = 4, fetch_mode = 'FLOAT')

    vbo = gpu.types.GPUVertBuf(len=len(verts), format=fmt)
    vbo.attr_fill(id="pos", data=verts)
    vbo.attr_fill(id="color", data=cols)
    
    batch = gpu.types.GPUBatch(type='LINE_STRIP', buf = vbo)
    shader.bind()
    # shader.uniform_float("color", (0.5, 1, 0.5, 0.15))
    batch.draw(shader)
    #Defaults
    bgl.glDisable(bgl.GL_LINE_SMOOTH)
    bgl.glDisable(bgl.GL_BLEND)

    area.tag_redraw()

class TIMERANGE_OT_proceduralrange_clear(bpy.types.Operator):
    """Clear range for procedural animation"""
    bl_idname = "timerange.proceduralrange_clear"
    bl_label = "Clear Procedural Range"

    def invoke(self, context, event):
        scene = context.scene
        if context.area.type in {"GRAPH_EDITOR", "DOPESHEET_EDITOR"}:
            pgh = scene.procedural_graph_handler
            if scene.procedural_graph.edit_mode:
                pgh.falloff_start = pgh.frame_start = pgh.frame_end = pgh.falloff_end = 0
                pgh.update_range()
            else:
                pgh.disable()
            return {'FINISHED'}
        else:
            self.report({'WARNING'}, "Somehow didn't run this in graph editor area")
            return {'CANCELLED'}

class TIMERANGE_OT_grab_timerange_handles(bpy.types.Operator):
    """Move timerange handles"""
    bl_idname = "timerange.grab_timerange_handles"
    bl_label = "Move the handles of the time range"

    mouse_x_start: IntProperty()

    def modal(self, context, event):
        context.area.tag_redraw()
        
        if event.type == 'MOUSEMOVE':
            x1,_ = context.region.view2d.region_to_view(self.mouse_x_start, 0)
            x2,_ = context.region.view2d.region_to_view(event.mouse_x, 0)
            offset = round(x2 - x1)
            for i in range(0, len(self.selected_handles)):
                handle = self.selected_handles[i]
                handle.frame = self.handle_frame_holds[i] + offset
                context.scene.procedural_graph_handler.update_range()
            return {'RUNNING_MODAL'}

        if event.type in {'ENTER', 'LEFTMOUSE'}:
            return {'FINISHED'}

        if event.type in {'ESC', 'RIGHTMOUSE'}:
            for i in range(0, len(self.selected_handles)):
                handle = self.selected_handles[i]
                handle.frame = self.handle_frame_holds[i]
                context.scene.procedural_graph_handler.update_range()
            # HACK: Just make it look good, fixes the falloff handles.
            # Repeats setting the value, or else the falloff's don't fix 
            # because they're reset before the inner values and idc to do it
            # properly here
            for i in range(0, len(self.selected_handles)):
                handle = self.selected_handles[i]
                handle.frame = self.handle_frame_holds[i]
                context.scene.procedural_graph_handler.update_range()
            return {'CANCELLED'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        self.mouse_x_start = event.mouse_x
        self.selected_handles = []
        self.handle_frame_holds = []

        for handle in context.scene.procedural_graph_handler.range.handles:
            if handle.select:
                self.selected_handles.append(handle)
                self.handle_frame_holds.append(handle.frame)

        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}


def pick_inside_two_handles(mouse_x, handle_1, handle_2, handle_1_pixel, handle_2_pixel):
    distance = handle_2_pixel - handle_1_pixel
    multiplier = 0.25
    reach_inside = distance * multiplier
    # reach outside_left
    if handle_1_pixel <= mouse_x <= (handle_1_pixel + reach_inside):
        return [handle_1]
    elif (handle_2_pixel - reach_inside) <= mouse_x <= handle_2_pixel:
        return [handle_2]
    else:
        return [handle_1, handle_2]
    return []

# Returns an array with TimeRange.handles depending on
# where the mouse lies within the timeline
def find_closest_time_range_handles(mouse_x, range, range_frames_region_pixels):
    # Outside the left falloff
    if range_frames_region_pixels[0] - 40 < mouse_x < range_frames_region_pixels[0]:
        return [range.handles[0]]

    # Determine the handles when mouse is between the handles on the left
    if range.handles[0].frame == range.handles[1].frame:
        if range_frames_region_pixels[0] - 10 <= mouse_x <= range_frames_region_pixels[1] + 10:
            return [range.handles[0], range.handles[1]]
    elif range_frames_region_pixels[0] <= mouse_x <= range_frames_region_pixels[1]:
        if range.handles[0].frame < range.handles[1].frame:
            return pick_inside_two_handles(
                mouse_x, 
                range.handles[0], range.handles[1], 
                range_frames_region_pixels[0], range_frames_region_pixels[1])

    # Determine the handles when mouse is between the handles in the middle
    if range.handles[1].frame == range.handles[2].frame:
        if range_frames_region_pixels[1] - 10 <= mouse_x <= range_frames_region_pixels[2] + 10:
            return [range.handles[1], range.handles[2]]
    elif range_frames_region_pixels[1] <= mouse_x <= range_frames_region_pixels[2]:
        if range.handles[1].frame < range.handles[2].frame:
            return pick_inside_two_handles(
                mouse_x, 
                range.handles[1], range.handles[2], 
                range_frames_region_pixels[1], range_frames_region_pixels[2])

    # Determine the handles when mouse is between the handles on the right
    if range.handles[2].frame == range.handles[3].frame:
        if range_frames_region_pixels[2] - 10 <= mouse_x <= range_frames_region_pixels[3] + 10:
            return [range.handles[2], range.handles[3]]
    elif range_frames_region_pixels[2] <= mouse_x <= range_frames_region_pixels[3]:
        if range.handles[2].frame < range.handles[3].frame:
            return pick_inside_two_handles(
                mouse_x, 
                range.handles[2], range.handles[3], 
                range_frames_region_pixels[2], range_frames_region_pixels[3])

    # Outside the right falloff
    if range_frames_region_pixels[3] < mouse_x < range_frames_region_pixels[3] + 40:
        return [range.handles[3]]

    return []

# Should never be called directly by the user
class TIMERANGE_OT_procedural_graph_editmode(bpy.types.Operator):
    """Toggles the UI handles for the Procedural Graph Editor"""
    bl_idname = "timerange.procedural_graph_editmode"
    bl_label = "Proof of concept for TimeRange editing"

    mouse_x_start: IntProperty()
    hold_handle: IntProperty()
    pressing: BoolProperty(default=False)
    left_mouse_pressed = False
    shift_pressed = False
    hover_frame = 0
    click_frame = 0
    # Quick HACK: to try handling offsetting animation data without access to 
    # the actual transforming operators/keyframing stuff within blender
    is_transforming = False
    _handle = None
    # Draw handles
    _handle_graph_sel_handle = None
    _handle_dope_sel_handle = None

    def add_selection_handles(self, context, event):
        if not self._handle_graph_sel_handle:
            self._handle_graph_sel_handle = bpy.types.SpaceGraphEditor.draw_handler_add(
                draw_selection_handle, (self, context), 'WINDOW', 'POST_PIXEL')

        if not self._handle_dope_sel_handle:
            self._handle_dope_sel_handle = bpy.types.SpaceDopeSheetEditor.draw_handler_add(
                draw_selection_handle, (self, context), 'WINDOW', 'POST_PIXEL')

    def remove_selection_handles(self, context, event):
        if self._handle_graph_sel_handle:
            bpy.types.SpaceGraphEditor.draw_handler_remove(self._handle_graph_sel_handle, 'WINDOW')
            self._handle_graph_sel_handle = None

        if self._handle_dope_sel_handle:
            bpy.types.SpaceDopeSheetEditor.draw_handler_remove(self._handle_dope_sel_handle, 'WINDOW')
            self._handle_dope_sel_handle = None

    def modal(self, context, event):
        # Get the area and region the mouse is over, else it could 
        # run calculations on the wrong region and area or do stuff when
        # not even in the graph editor
        area = None
        region = None
        for a in context.screen.areas:
            if a.type not in {'GRAPH_EDITOR', 'DOPESHEET_EDITOR', 'VIEW_3D'}:
                continue
            for r in a.regions:
                if r.type == 'WINDOW':
                    if (event.mouse_x >= r.x and
                        event.mouse_y >= r.y and
                        event.mouse_x < r.width + r.x and
                        event.mouse_y < r.height + r.y
                    ):
                        area = a
                        region = r
                        break
        if not area:
            self.remove_selection_handles(context, event)
            return {'PASS_THROUGH'}
        #Make sure to pass through if in view_3d
        if area.type == 'VIEW_3D':
            if event.type == 'G' and not True in {event.shift, event.alt, event.ctrl}:
                self.is_transforming = True
                pg = context.scene.procedural_graph
                pgh = context.scene.procedural_graph_handler
                pg.offset_start = context.active_pose_bone.location
                pg.play_hold = context.scene.frame_current
                pg.auto_key_hold = context.scene.tool_settings.use_keyframe_insert_auto
                context.scene.tool_settings.use_keyframe_insert_auto = False
                context.scene.frame_current = pgh.start
                pg.fcurves.clear()
                pg.fghosts.clear()
                pg.ecurves.clear()
                pg.key_hold.clear()
                active_bone_paths = []
                #Get selected pose bones since fcurves can be selected even when they're not displaying in the graph editor
                for pb in bpy.context.selected_pose_bones:
                    active_bone_paths.append('pose.bones["%s"]' % pb.name)
                for fc in bpy.context.active_object.animation_data.action.fcurves:
                    #Make sure the fcurve is selected, has correct data path, and is not hidden
                    # print(fc.data_path.rsplit('.', 1))
                    tmp_arr = []
                    for i in range(pgh.start, pgh.end+1):
                        tmp_arr.append(fc.evaluate(i))
                    pg.ecurves.append(tmp_arr)
                    if fc.data_path.rsplit('.', 1)[-1] == 'location':
                    # if fc.data_path.rsplit('.', 1)[-1] == 'location' and fc.array_index == 0:
                        pg.fcurves.append(fc)
                        pg.fghosts.append(FGhost(fc, pgh))
                        pg.key_hold.append(tmp_arr[0])
                #Build curves to restor upon
                for fc in pg.fcurves:
                    kp_arr = []
                    for kp in fc.keyframe_points:
                        if pgh.start <= kp.co[0] <= pgh.end:
                            kp_arr.append((kp.co[0], kp.co[1]))
                    pg.hcurves.append(kp_arr)

                self._handle = bpy.types.SpaceGraphEditor.draw_handler_add(
                    draw_curves, (), 
                    'WINDOW', 
                    'POST_PIXEL')
                bpy.app.timers.register(transform_update, first_interval=0.01667)

            if event.type in {'RIGHTMOUSE', 'LEFTMOUSE'} and self.is_transforming:
                pg = bpy.context.scene.procedural_graph
                pgh = context.scene.procedural_graph_handler
                context.scene.frame_current = pg.play_hold
                context.scene.tool_settings.use_keyframe_insert_auto = pg.auto_key_hold
                for fghost in pg.fghosts:
                    if event.type == 'RIGHTMOUSE':
                        fghost.clean(cancelled=True)
                    else:
                        fghost.clean()
                pg.fghosts.clear()
                for i in range(0, len(pg.fcurves)):
                    #Overwrite the getting transformed keyframe
                    fc = pg.fcurves[i]
                    insert_keyframe_point(
                        fcurve=fc,
                        frame=pgh.start, 
                        value=pg.key_hold[i],
                        options={"NEEDED"}, 
                        keyframe_type="KEYFRAME",
                        deselect=True)
                self.is_transforming = False
                bpy.app.timers.unregister(transform_update)
                if self._handle:
                    bpy.types.SpaceGraphEditor.draw_handler_remove(self._handle, 'WINDOW')
                    self._handle = None
                for a in context.screen.areas:
                    a.tag_redraw()
            return{'PASS_THROUGH'}
        
        override = bpy.context.copy()
        override['area'] = area
        override['region'] = region

        area.tag_redraw()

        pgh = bpy.types.Scene.procedural_graph_handler
        pgh.mouse_x = event.mouse_x - region.x
        pgh.mouse_y = event.mouse_y - region.y

        if not region:
            print("FUCK")
            return {'PASS_THROUGH'}

        (handle_0_x, _) = region.view2d.view_to_region(
            pgh.range.handles[0].frame, 0, clip=False)
        (handle_1_x, _) = region.view2d.view_to_region(
            pgh.range.handles[1].frame, 0, clip=False)
        (handle_2_x, _) = region.view2d.view_to_region(
            pgh.range.handles[2].frame, 0, clip=False)
        (handle_3_x, _) = region.view2d.view_to_region(
            pgh.range.handles[3].frame, 0, clip=False)

        handles_in_region_pixels = []
        for handle in pgh.range.handles:
            x,_ = region.view2d.view_to_region(
                handle.frame, 0, clip=False)
            
            handles_in_region_pixels.append(x)
                
        bottom = 0
        top = area.height-49

        # Select / Deselect all
        if event.type == 'A' and event.value == 'PRESS':
            if event.alt:
                for h in pgh.range.handles:
                    h.select = False
                return {'RUNNING_MODAL'}
            for h in pgh.range.handles:
                h.select = True
            return {'RUNNING_MODAL'}

        if event.type == 'G' and event.value == 'PRESS':
            bpy.ops.timerange.grab_timerange_handles(override, 'INVOKE_DEFAULT')

        if event.type == 'LEFTMOUSE':
            if event.value == 'PRESS':
                self.left_mouse_pressed = True

                if event.shift:
                    x, y = region.view2d.region_to_view(
                        event.mouse_x - region.x, event.mouse_y - region.y)
                    self.click_frame = self.hover_frame = round(x)

                    pgh.update_range()
                else:
                    for h in pgh.range.handles:
                        h.select = False

                ## Determine which handles to select ##
                for handle in find_closest_time_range_handles(
                    event.mouse_x - region.x, pgh.range, handles_in_region_pixels):
                    handle.select = True
                # # Handle 0 and 1, left-most two. The left falloff range
                # if is_in_rect((event.mouse_x - region.x, event.mouse_y - region.y), handle_0_x, bottom, handle_1_x, top):
                #     pgh.range.handles[0].select = pgh.range.handles[1].select = True
                # # Handle 1 and 2, middle two. The fully influenced range
                # elif is_in_rect((event.mouse_x - region.x, event.mouse_y - region.y), handle_1_x, bottom, handle_2_x, top):
                #     pgh.range.handles[1].select = pgh.range.handles[2].select = True
                # # Handle 2 and 3, right-most two. The right falloff range
                # elif is_in_rect((event.mouse_x - region.x, event.mouse_y - region.y), handle_2_x, bottom, handle_3_x, top):
                #     pgh.range.handles[2].select = pgh.range.handles[3].select = True
                        
                return {'RUNNING_MODAL'}
            elif event.value == 'RELEASE':
                self.left_mouse_pressed = False
            return {'RUNNING_MODAL'}

        if event.type == 'MOUSEMOVE':
            if event.shift:
                x, y = region.view2d.region_to_view(
                    event.mouse_x - region.x, event.mouse_y - region.y)
                self.hover_frame = round(x)
                # Set the time's range, removing falloffs
                if self.left_mouse_pressed:
                    for h in pgh.range.handles:
                        h.select = False

                    pgh.range.handles[0].frame = pgh.range.handles[1].frame = self.click_frame if self.hover_frame > self.click_frame else self.hover_frame
                    pgh.range.handles[2].frame = pgh.range.handles[3].frame = self.hover_frame if self.hover_frame > self.click_frame else self.click_frame
                    pgh.update_range()
                    return {'RUNNING_MODAL'}

            if self.left_mouse_pressed:
                bpy.ops.timerange.grab_timerange_handles(override, 'INVOKE_DEFAULT')
                self.left_mouse_pressed = False
                return {'RUNNING_MODAL'}

        if event.type == 'TAB' and event.value == 'PRESS':
            #Change Left Easing Type
            if is_in_rect((event.mouse_x - region.x, event.mouse_y - region.y), handle_0_x, top, handle_1_x, area.height):
                pgh.falloff_left_easing_index = (pgh.falloff_left_easing_index + 1) % len(easing_types)
                pgh.falloff_left_easing = easing_types[pgh.falloff_left_easing_index][0]
                return {'RUNNING_MODAL'}
            #Change Right Easing Type
            elif is_in_rect((event.mouse_x - region.x, event.mouse_y - region.y), handle_2_x, top, handle_3_x, area.height):
                pgh.falloff_right_easing_index = (pgh.falloff_right_easing_index + 1) % len(easing_types)
                pgh.falloff_right_easing = easing_types[pgh.falloff_right_easing_index][0]
                return {'RUNNING_MODAL'}

        if event.type in {'WHEELUPMOUSE', 'WHEELDOWNMOUSE'}:
            direction = 1 if event.type == 'WHEELUPMOUSE' else -1
            #Change interpolation type #TODO: Finish this
            if event.ctrl:
                #Change Left Interpolation Type
                if is_in_rect((event.mouse_x - region.x, event.mouse_y - region.y), handle_0_x, top, handle_1_x, area.height):
                    pgh.falloff_left_interpolation_index = (pgh.falloff_left_interpolation_index + direction) % len(interpolation_modes)
                    pgh.falloff_left_interpolation = interpolation_modes[pgh.falloff_left_interpolation_index][0]
                    return {'RUNNING_MODAL'}
                #Change Right Interpolation Type
                elif is_in_rect((event.mouse_x - region.x, event.mouse_y - region.y), handle_2_x, top, handle_3_x, area.height):
                    pgh.falloff_right_interpolation_index = (pgh.falloff_right_interpolation_index + direction) % len(interpolation_modes)
                    pgh.falloff_right_interpolation = interpolation_modes[pgh.falloff_right_interpolation_index][0]
                    return {'RUNNING_MODAL'}

            #Grow or shrink the falloff ranges
            if event.shift:
                if pgh.start == pgh.end:
                    # pgh.falloff_start = pgh.frame_start = pgh.frame_end = pgh.falloff_end = self.hover_frame
                    pgh.range.handles[0].frame = pgh.range.handles[1].frame = pgh.range.handles[2].frame = pgh.range.handles[3].frame = self.hover_frame
                # pgh.falloff_start -= direction
                pgh.range.handles[0].frame -= direction
                # pgh.falloff_end += direction
                pgh.range.handles[3].frame += direction
                pgh.update_range()
                return {'RUNNING_MODAL'}

        #Handle the draw handles
        if event.shift:
            self.hover_frame,_ = region.view2d.region_to_view(event.mouse_x - region.x, 0)
            self.hover_frame = round(self.hover_frame)
            self.add_selection_handles(context, event)
        else:
            self.remove_selection_handles(context, event)

            if self._handle:
                bpy.types.SpaceGraphEditor.draw_handler_remove(self._handle, 'WINDOW')
                self._handle = None

        if event.type in {'ESC'}:
            if event.value == 'PRESS' and not True in {event.ctrl, event.shift, event.alt}:
                context.scene.procedural_graph.edit_mode = False

        if context.scene.procedural_graph.edit_mode == False:
            if self.left_mouse_pressed:
                #Select Handle
                return {'RUNNING_MODAL'}
            return {'FINISHED'}
        return {'PASS_THROUGH'}

    def invoke(self,context, event):
        if context.area:
            context.area.tag_redraw()
        if context.scene.procedural_graph.edit_mode:
            # context.scene.procedural_graph.edit_mode = True
            context.window_manager.modal_handler_add(self)
            pgh = bpy.types.Scene.procedural_graph_handler
            if not pgh.enabled:
                pgh.enable()
            return {'RUNNING_MODAL'}
        return {'CANCELLED'}

class TIMERANGE_OT_toggle_proceduralrange_mode(bpy.types.Operator):
    """Toggles the UI handles for the Procedural Graph Editor"""
    bl_idname = "timerange.toggle_proceduralrange_mode"
    bl_label = "Toggle Procedural Range Mode"

    def invoke(self, context, event):
        context.scene.procedural_graph.edit_mode = not context.scene.procedural_graph.edit_mode
        return {'FINISHED'}

def falloff_type_draw_item(self, context):
    preferences = context.preferences.addons[__package__.split(".")[0]].preferences
    # pg = context.scene.procedural_graph_handler
    procedural_graph = context.scene.procedural_graph
    layout = self.layout
    row = layout.row(align=True)
    row.prop(procedural_graph, "edit_mode", icon_only=True, icon='STATUSBAR')
    row.prop(preferences, "interpolation_type", text="")
    row.prop(preferences, "easing_type", text="", icon_only=True)
    row.prop(preferences, "sync_preview_range", icon_only=True, icon='UV_SYNC_SELECT')

#region    #Registration#
classes = [
    ProceduralFcurve_Settings,
    ProceduralGraph_Settings,
    TIMERANGE_OT_toggle_proceduralrange_mode,
    #Operators
    TIMERANGE_OT_procedural_graph_editmode,
    TIMERANGE_OT_grab_timerange_handles,
    TIMERANGE_OT_proceduralrange_clear
]

addon_keymaps = []

def register():
    #Register classes
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.procedural_graph_handler = Procedural_Graph_Handler()
    bpy.types.Scene.procedural_graph = PointerProperty(
        type=ProceduralGraph_Settings,
        name="ProceduralGraph"
    )

    #Append to graph header
    bpy.types.GRAPH_HT_header.append(falloff_type_draw_item)
    bpy.types.DOPESHEET_HT_header.append(falloff_type_draw_item)

    ##Add shortcuts
    wm = bpy.context.window_manager
    kc = wm.keyconfigs.addon
    if kc:
        km = kc.keymaps.new(name='Dopesheet Generic', space_type='DOPESHEET_EDITOR')
        kmi = km.keymap_items.new("timerange.toggle_proceduralrange_mode", type='Z', value='PRESS')
        addon_keymaps.append((km, kmi))
        kmi = km.keymap_items.new("timerange.proceduralrange_clear", type='Z', value='PRESS', alt=True)
        addon_keymaps.append((km, kmi))

        km = kc.keymaps.new(name='Graph Editor Generic', space_type='GRAPH_EDITOR')
        kmi = km.keymap_items.new("timerange.toggle_proceduralrange_mode", type='Z', value='PRESS')
        addon_keymaps.append((km, kmi))
        kmi = km.keymap_items.new("timerange.proceduralrange_clear", type='Z', value='PRESS', alt=True)
        addon_keymaps.append((km, kmi))
    
def unregister():
    bpy.types.Scene.procedural_graph_handler.disable()
    bpy.context.scene.procedural_graph.edit_mode = False
    del bpy.types.Scene.procedural_graph_handler
    del bpy.types.Scene.procedural_graph
    #Remove from headers
    bpy.types.GRAPH_HT_header.remove(falloff_type_draw_item)
    bpy.types.DOPESHEET_HT_header.remove(falloff_type_draw_item)

    #Unregister classes
    for cls in classes:
        bpy.utils.unregister_class(cls)

    #Remove shortcuts
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()

#endregion #Registration#

if __name__ == "__main__":
    try:
        unregister()
    except RuntimeError as e:
        print(e)
    except AttributeError as e:
        print(e)
    except:
        import sys
        print("Error:", sys.exc_info()[0])
        pass
    register()

    # bpy.context.scene.procedural_graph.edit_mode = True

# Procedural Fcurve Editor, Adds tools to make modifying fcurves easier.
# Copyright (C) 2020 bird_d <relay198@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import bpy
import gpu
from gpu_extras.batch import batch_for_shader
import bgl
import blf
import logging
import time
from math import sin, cos, pi, pow, sqrt
log = logging.getLogger("%s" % (__name__))

#Goals of this
# Should handle hiding original FCurve and unhiding after modal 
#   is completed, aka in def (__init__/clean):
# Should handle the current keyframes for an fcurve, so that 
#   it can be reapplied on cancel.
# Should be able to add correct data to a buffer that will draw lines (pos and color)
class FGhost():
    #Should become reference to fcurve
    fcurve = None
    evaluated_frames = []
    offsets = []
    rehide = False
    def __init__(self, fcurve, pgh):
        self.fcurve = fcurve
        self.rehide = fcurve.hide
        self.fcurve.hide = True
        for frame_index in range(pgh.start, pgh.end):
            self.evaluated_frames.append(self.fcurve.evaluate(frame_index))

    def add_to_vert_buffer(self, vert_arr, color_arr):
        pgh = bpy.context.scene.procedural_graph_handler
        points = []
        for i in range(0, len(self.fcurve.keyframe_points)):
            #TODO: Make keyframe after area's x be last to add to buffer, then leave
            kp = self.fcurve.keyframe_points[i]
            col = [self.fcurve.color.r, self.fcurve.color.g, self.fcurve.color.b, 0.7]
            x, y = (None, None)
            if kp.co[0] == pgh.start:
                x, y = bpy.context.region.view2d.view_to_region(
                    kp.co[0], 
                    self.evaluated_frames[int(kp.co[0])-pgh.start], clip=False
                )
            else:
                x, y = bpy.context.region.view2d.view_to_region(kp.co[0], kp.co[1], clip=False)
            if i == 0:
                vert_arr.append((-9999999, y))
                color_arr.append(col)
                points.append(vert_arr[-1])
            vert_arr.append((x, y))
            points.append(vert_arr[-1])
            color_arr.append(col)
            if i == len(self.fcurve.keyframe_points)-1:
                # if len(self.fcurve.keyframe_points) == 0:
                #     vert_arr.append((x, y))
                #     color_arr.append(col)
                vert_arr.append((9999999, y))
                points.append(vert_arr[-1])
                color_arr.append((0,0,0,0))
    def clean(self, cancelled=False):
        self.fcurve.hide = self.rehide

#Easing, for use with a value between 0 and 1
#Might not be the best implementation for this, just assumes you use 0-1 values
#Based on Robert Penner's easing functions and easings.net
class Ease():
    #Sineusoidal
    @staticmethod
    def in_sine(x):
        return 1 - cos((x * pi) / 2)
    @staticmethod
    def out_sine(x):
        return sin((x * pi) / 2 )
    @staticmethod
    def in_out_sine(x):
        return -(cos(x * pi) - 1) / 2
    #Quadratic
    @staticmethod
    def in_quad(x):
        return x * x
    @staticmethod
    def out_quad(x):
        return 1 - (1 - x) * (1 - x)
    @staticmethod
    def in_out_quad(x):
        return 2 * x * x if x < 0.5 else 1 - pow(-2 * x + 2, 2) / 2
    #Cubic
    @staticmethod
    def in_cubic(x):
        return x * x * x
    @staticmethod
    def out_cubic(x):
        return 1 - pow(1 - x, 3)
    @staticmethod
    def in_out_cubic(x):
        return 4 * x * x * x if x < 0.5 else 1 - pow(-2 * x + 2, 3) / 2
    #Quartic
    @staticmethod
    def in_quart(x):
        return x * x * x * x
    @staticmethod
    def out_quart(x):
        return 1 - pow(1 - x, 4)
    @staticmethod
    def in_out_quart(x):
        return  8 * x * x * x * x if x < 0.5 else 1 - pow(-2 * x + 2, 4) / 2
    #Quintic
    @staticmethod
    def in_quint(x):
        return x * x * x * x * x
    @staticmethod
    def out_quint(x):
        return 1 - pow(1 - x, 5)
    @staticmethod
    def in_out_quint(x):
        return  16 * x * x * x * x * x if x < 0.5 else 1 - pow(-2 * x + 2, 5) / 2
    #Exponential
    @staticmethod
    def in_expo(x):
        return 0 if x == 0 else pow(2, 10 * x - 10)
    @staticmethod
    def out_expo(x):
        return 1 if x == 1 else 1 - pow(2, -10 * x)
    @staticmethod
    def in_out_expo(x):
        return 0 if x == 0 else 1 if x == 1 else pow(2, 20 * x - 10) / 2 if x < 0.5 else (2 - pow(2, -20 * x + 10)) / 2
    #Circular
    @staticmethod
    def in_circ(x):
        return 1 - sqrt(1 - pow(x, 2))
    @staticmethod
    def out_circ(x):
        return sqrt(1 - pow(x - 1, 2))
    @staticmethod
    def in_out_circ(x):
        return (1 - sqrt(1 - pow(2 * x, 2))) / 2 if x < 0.5 else (sqrt(1 - pow(-2 * x + 2, 2)) + 1) / 2
    #Back
    def in_back(x):
        c1 = 1.70158
        c3 = c1 + 1
        return c3 * x * x * x - c1 * x * x
    @staticmethod
    def out_back(x):
        c1 = 1.70158
        c3 = c1 + 1
        return 1 + c3 * pow(x - 1, 3) + c1 * pow(x - 1, 2)
    @staticmethod
    def in_out_back(x):
        c1 = 1.70158
        c2 = c1 * 1.525
        return (pow(2 * x, 2) * ((c2 + 1) * 2 * x - c2)) / 2 if x < 0.5 else (pow(2 * x - 2, 2) * ((c2 + 1) * (x * 2 - 2) + c2) + 2) / 2
    @staticmethod
    def in_elastic(x):
        c4 = (2 * pi) / 3
        return 0 if x == 0 else 1 if x == 1 else -pow(2, 10 * x - 10) * sin((x * 10 - 10.75) * c4)
    @staticmethod
    def out_elastic(x):        
        c4 = (2 * pi) / 3
        return 0 if x == 0 else 1 if x == 1 else pow(2, -10 * x) * sin((x * 10 - 0.75) * c4) + 1
    @staticmethod
    def in_out_elastic(x):
        c5 = (2 * pi) / 4.5
        return 0 if x == 0 else 1 if x == 1 else -(pow(2, 20 * x - 10) * sin((20 * x - 11.125) * c5)) / 2 if x < 0.5 else (pow(2, -20 * x + 10) * sin((20 * x - 11.125) * c5)) / 2 + 1
    @staticmethod
    def in_bounce(x):
        return 1 - Ease.out_bounce(1 - x)
    @staticmethod
    def out_bounce(x):
        n1 = 7.5625
        d1 = 2.75

        if (x < 1 / d1):
            return n1 * x * x
        elif (x < 2 / d1):
            tmp = (x - 1.5 / d1)
            return n1 * tmp * tmp + 0.75
        elif (x < 2.5 / d1):
            tmp = (x - 2.25 / d1)
            return n1 * tmp * tmp + 0.9375
        tmp = (x - 2.625 / d1)
        return n1 * tmp * tmp + 0.984375
    @staticmethod
    def in_out_bounce(x):
        return (1 - Ease.out_bounce(1 - 2 * x)) / 2 if x < 0.5 else (1 + Ease.out_bounce(2 * x - 1)) / 2
#Helpful functions
def clamp(val, lower=0, upper=1):
    return lower if val < lower else upper if val > upper else val
def get_active_fcurves(context):
    start_time = time.time()
    #HACK: People are more likely to be using this addon for bone animation, so I'll just support that for now
    fcurves = []
    active_bone_paths = []
    #Get selected pose bones since fcurves can be selected even when they're not displaying in the graph editor
    for pb in context.selected_pose_bones:
        active_bone_paths.append('pose.bones["%s"]' % pb.name)
    for fc in context.active_object.animation_data.action.fcurves:
        #Make sure the fcurve is selected, has correct data path, and is not hidden
        if fc.select and fc.data_path.rsplit('.', 1)[0] in active_bone_paths and not fc.hide:
            fcurves.append(fc)
    log.debug("Time to get Fcurves: %.8fs" % (time.time() - start_time))
    log.debug("Fcurves: %d" % len(fcurves))
    return fcurves
def normalize(val, min_val, max_val):
    if min_val == max_val:
        return val
    return ((val - min_val) / (max_val - min_val))
def scale(scale, min_val, max_val):
    return (scale * (max_val - min_val)) + min_val
def get_preferences():
    return bpy.context.preferences.addons[__package__].preferences
## Common fcurve code
def handle_common_errors(self):
    if bpy.context.area.type != 'GRAPH_EDITOR':
        self.report({'WARNING'}, "Somehow didn't run this in graph editor area")
        return False
    pg = bpy.types.Scene.procedural_graph_handler
    if pg.start == pg.end:
        self.report({'WARNING'}, "No area selected to modify.")
        return False
    if len(self.fcurves) == 0:
        self.report({'WARNING'}, "Must have Fcurve channel(s) selected.")
        return False
    return True

def insert_keyframe_point(fcurve, frame, value, options, keyframe_type='KEYFRAME', deselect=False):
    kp = fcurve.keyframe_points.insert(frame=frame, value=value, options=options, keyframe_type=keyframe_type)

    if deselect:
        kp.select_control_point = False
        kp.select_left_handle = False
        kp.select_right_handle = False
    return kp

def setup_fcurve_restore(self, fcurve):
    pg = bpy.types.Scene.procedural_graph_handler
    kp_arr = []
    for kp in fcurve.keyframe_points:
        if pg.start <= kp.co[0] <= pg.end:
            kp_arr.append((kp.co[0], kp.co[1]))
    self.Hfcurves.append(kp_arr)

def is_in_rect(point, x1, y1, x2, y2):
    return ((x1 <= point[0] <= x2) and (y1 <= point[1] <= y2))

def restore_fcurves(self):
    #self.fcurves should probably just be changed to use anim_data.fcurves.
    deselect = get_preferences().deselect_new_keyframes
    for i in range(0, len(self.fcurves)):
        fc = self.fcurves[i]
        pg = bpy.types.Scene.procedural_graph_handler
        for kp_i in range(pg.start, pg.end+1):
            keyframe = None
            for kp in fc.keyframe_points:
                if kp.co[0] == kp_i:
                    keyframe = kp
            fc.keyframe_points.remove(keyframe, fast=True)
        for co in self.Hfcurves[i]:
            insert_keyframe_point(fc, frame=co[0], value=co[1], options={"NEEDED"}, deselect=deselect)

## Custom drawing
def draw_box_from_points(x=0, y=0, x2=200, y2=100, col=(1, 0, 0.75, 1.0)):
    shader = gpu.shader.from_builtin('2D_UNIFORM_COLOR')
    
    bgl.glEnable(bgl.GL_BLEND)

    #print("x: ", x, ", x2: ", x2)
    verts = [(x,y), (x2,y), (x2,y2), (x,y2)]
    faces = [ verts[0], verts[1], verts[2], verts[3], verts[2], verts[0] ]
    batch = batch_for_shader(shader, 'TRIS', {"pos": faces})
    shader.bind()
    shader.uniform_float("color", col)
    batch.draw(shader)
    
    #Defaults
    bgl.glDisable(bgl.GL_BLEND)

def draw_curved_rectangle(x1, y1, x2, y2, col):

    shader = gpu.shader.from_builtin('2D_UNIFORM_COLOR')

    fmt = gpu.types.GPUVertFormat()
    fmt.attr_add(id = "pos", comp_type = 'F32', len = 2, fetch_mode = 'FLOAT')

    verts = [(x,11), (x,context.area.height)]

    vbo = gpu.types.GPUVertBuf(len=len(verts), format=fmt)
    vbo.attr_fill(id="pos", data=verts)
    
    batch = gpu.types.GPUBatch(type='LINE_STRIP', buf = vbo)
    
    shader.bind()
    shader.uniform_float("color", (0.368627, 0.701961, 0.407843, 1.0))
    
    batch.draw(shader)


def draw_handle_selection(frame, col, context):
    (x, _) = context.region.view2d.view_to_region(frame, 0, clip=False)
    y = context.area.height-36
    draw_line(x, 0, x, y, col)
    width, height = (24, 20)
    draw_box_from_points(x - (width/2), y - (height/2), x + (width/2) + 1, y + (height/2) - 3, col)

    #Text
    font_id = 0

    size, dpi = (28, 28)
    blf.size(0, size, dpi)
    width, height = (blf.dimensions(0, str(frame)))

    font_x = x - (width/2)
    font_y = y - (height/2)
    blf.position(font_id, font_x, font_y-1, 0)
    blf.color(0, 1.0, 1.0, 1.0, 1.0)
    blf.draw(0, str(frame))

# "Hover handle", too lazy to change name and then change all the other code

def draw_selection_handle(self, context):
    if not get_preferences().show_hover_handle:
        return
    (x, _) = context.region.view2d.view_to_region(self.hover_frame, 0, clip=False)
    y = context.area.height-36
    col = (0.368627, 0.701961, 0.407843, 1.0)
    draw_line(x, 0, x, y, col)
    width, height = (24, 20)
    draw_box_from_points(x - (width/2), y - (height/2), x + (width/2) + 1, y + (height/2) - 3, col)

    #Text
    font_id = 0

    size, dpi = (28, 28)
    blf.size(font_id, size, dpi)
    width, height = (blf.dimensions(font_id, str(self.hover_frame)))

    font_x = x - (width/2)
    font_y = y - (height/2)
    blf.position(font_id, font_x, font_y-1, 0)
    blf.color(font_id, 1.0, 1.0, 1.0, 1.0)
    blf.draw(font_id, str(self.hover_frame))

    # Don't draw influence if option disabled
    if not get_preferences().show_hover_handle_influence:
        return

    # Draw influence box
    width, height = (28, 20)
    tmp = 20
    draw_box_from_points(
        x - (width/2), (y - (height/2)) - tmp, 
        x + (width/2) + 1, (y + (height/2) - 3) - tmp, col)

    frame_influence = bpy.context.scene.procedural_graph_handler.get_frame_influence(
                self.hover_frame)

    # Draw influence text
    width, height = (blf.dimensions(font_id, "%.2f" % frame_influence))

    font_x = x - (width/2)
    font_y = y - (height/2)
    blf.position(font_id, font_x, (font_y-1) - tmp, 0)
    blf.color(font_id, 1.0, 1.0, 1.0, 1.0)
    blf.draw(font_id, "%.2f" % frame_influence)

def draw_line(x1, y1, x2, y2, col):
    shader = gpu.shader.from_builtin('2D_UNIFORM_COLOR')

    fmt = gpu.types.GPUVertFormat()
    fmt.attr_add(id = "pos", comp_type = 'F32', len = 2, fetch_mode = 'FLOAT')

    verts = [(x1,y1), (x2,y2)]

    vbo = gpu.types.GPUVertBuf(len=len(verts), format=fmt)
    vbo.attr_fill(id="pos", data=verts)
    
    batch = gpu.types.GPUBatch(type='LINE_STRIP', buf = vbo)
    
    shader.bind()
    shader.uniform_float("color", col)
    
    batch.draw(shader)

def draw_gradient_box_from_points(x=0, y=0, x2=200, y2=100, col=(1, 0, 0.75, 1.0), col2=(1,0,0,1)):
    shader = gpu.shader.from_builtin('2D_SMOOTH_COLOR')
    
    bgl.glEnable(bgl.GL_BLEND)

    verts = [(x,y), (x2,y), (x2,y2), (x,y2)]
    faces = [ verts[0], verts[1], verts[2], verts[0], verts[2], verts[3] ]
    vcols = [col2, col, col, col2, col, col2]
    batch = batch_for_shader(shader, 'TRIS', {"pos": faces, "color": vcols})
    shader.bind()
    batch.draw(shader)
    
    #Defaults
    bgl.glDisable(bgl.GL_BLEND)